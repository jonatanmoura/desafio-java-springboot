package com.jonatan.productms.repository.product;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.util.StringUtils;

import com.jonatan.productms.model.Product;
import com.jonatan.productms.repository.filter.ProductFilter;

public class ProductRepositoryImpl  implements ProductRepositoryQuery{
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Product> filter(ProductFilter productFilter) {
		CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Product> criteriaQuery = criteriabuilder.createQuery(Product.class);		
		Root<Product> root = criteriaQuery.from(Product.class);
		
		Predicate[] preticates = createRestrictions(productFilter, criteriabuilder, root);
		criteriaQuery.where(preticates);
		
		TypedQuery<Product> typedQuery = entityManager.createQuery(criteriaQuery);
		
		return typedQuery.getResultList();
	}

	private Predicate[] createRestrictions(ProductFilter productFilter, CriteriaBuilder criteriabuilder,
			Root<Product> root) {
		productFilter.toString();
		List<Predicate> listPredicate = new ArrayList<>();
		
		if(StringUtils.hasLength(productFilter.getQ())) {
			Predicate predicateName = criteriabuilder.like(
					criteriabuilder.lower(root.get("name")), "%" + productFilter.getQ().toLowerCase() + "%");
			Predicate predicateDescription = criteriabuilder.like(
					criteriabuilder.lower(root.get("description")), "%" + productFilter.getQ().toLowerCase() + "%");
			listPredicate.add(criteriabuilder.or(predicateName,predicateDescription));
			
		}		
		
		 if(productFilter.getMin_price() != null) {
			listPredicate.add(criteriabuilder.and(criteriabuilder.greaterThanOrEqualTo(root.get("price"), productFilter.getMin_price())));
		}
		 if(productFilter.getMax_price() != null) {
			listPredicate.add(criteriabuilder.and(criteriabuilder.lessThanOrEqualTo(root.get("price"), productFilter.getMax_price())));
		}
		
		return listPredicate.toArray(new Predicate[listPredicate.size()]);
	}

}
