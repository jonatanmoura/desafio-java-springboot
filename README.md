MS – Criado para a manipulação de produtos: Para a Compasso UOL

Tecnologias Utilizadas 


-H2 Database para o Banco de Dados (Foi utilizado para facilitar os testes da API)
-Flywaydb versão 7.3 Foi utilizado para criação automaticamente do schema e da tabela no banco de dados h2  (Não foi utilizado o DDl generate true)
-Spring Boot Versão 2.5.5 Para criação do MS
-Java Versão 8 Para codificação
-Hibernate para o ORM
-Swagger versão 3.0 Para fazer a Documentação da API

Links:

 (Swagger) http://localhost:9999/swagger-ui/#/
( Console H2) http://localhost:9999/h2-consoledb

